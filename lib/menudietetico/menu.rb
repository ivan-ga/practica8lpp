class Menu
    def initialize(titulo, menu, porciones, gramos, porcentajes)
        @titulo = titulo
        @menu = menu
        @porciones = porciones
        @gramos = gramos
        @porcentajes = porcentajes
        @contador = 0
        
    end
    
    def mostrar_plato
         out = "#{@menu[0]}, " + "#{@porciones[0]}, " + "#{@gramos[0]}"
         return out
    end
    
    def get_ingesta
         out = "#{@titulo[1]}"
         return out
    end
    
    def get_platos
        @explicacion = ""
         @menu.each do |item2|
            @explicacion << "- #{item2}, #{@porciones[@contador]}, #{@gramos[@contador]}\n"
            @contador+=1
        end
       return "#{@explicacion}"
    end
    
    def get_vct
        out = "#{@porcentajes[0]}"
         return out
    end
    
    def get_proteinas
        out = "#{@porcentajes[1]}"
         return out
    end
    
    def get_grasas
        out = "#{@porcentajes[2]}"
         return out
    end
    
    
    def get_hidratos
        out = "#{@porcentajes[3]}"
         return out
    end
    
     def to_s
        @explicacion = "#{@titulo[0]} " + "#{@titulo[1]}\n"
         @menu.each do |item2|
            @explicacion << "- #{item2}, #{@porciones[@contador]}, #{@gramos[@contador]}\n"
            @contador+=1
        end
        
        @explicacion << "#{@porcentajes[0]}" + "#{@porcentajes[1]}-" + "#{@porcentajes[2]}-" + "#{@porcentajes[3]}\n"
       return "#{@explicacion}"
    end
    
    attr_accessor :titulo
    attr_accessor :menu
    attr_accessor :porciones
    attr_accessor :gramos
    attr_accessor :porcentajes
   
end

class Grupos_alimentos < Menu
    
    def initialize(texto, titulo, menu, porciones, gramos, porcentajes)
        @texto = texto
        super(titulo, menu, porciones, gramos, porcentajes)
    end
    
    def to_s
        imprime = super.to_s + "Menu del grupo: #{texto}\n"
        return imprime
    end
    
    attr_accessor :texto
end

class Edades < Menu
    def initialize(texto, titulo, menu, porciones, gramos, porcentajes)
        @texto = texto
        super(titulo, menu, porciones, gramos, porcentajes)
    end
    
    def to_s
        imprime = super.to_s + "Edad: #{texto}\n"
        return imprime
    end
    
    attr_accessor :texto
end