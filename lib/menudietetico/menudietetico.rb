#require "menudietetico/menu.rb"

Node = Struct.new(:value, :next, :previo)

class Lista
    def initialize
        @inicio = nil
        @final = nil
    end
    
    def get_inicio
       return @inicio 
    end
    
    def get_final
       return @final 
    end
    
    def is_empty
        # @inicio == nil
        r = false
        if(@inicio == nil)
            r = true
        elsif
            r = false
        end
        return r
    end
    
    def insertar(elemento) #inserta por el inicio
        nodo_aux = Node.new(elemento, nil, nil)
        if(is_empty())
            @inicio = nodo_aux
            @final = nodo_aux
            @inicio.next = nil
            @inicio.previo = nil
            #@final = @inicio
        elsif
            aux = @inicio
            @inicio = nodo_aux
            @inicio.next = aux
            @inicio.previo = nil
            aux.previo = @inicio
        end
    end
    
    def insertar_final(elemento)
        nodo_aux = Node.new(elemento, nil, nil)
        if(is_empty())
            @inicio = nodo_aux
            @final = nodo_aux
            @inicio.next = nil
            @inicio.previo = nil
            @final.previo = nil
            @final.next = nil
            #@final = @inicio
        elsif
            aux = @final
            @final = nodo_aux
            @final.previo = aux
            @final.next = nil
        end
    end
    
    def insertar_muchos(vector) #inserta muchos por el inicio
        i=0
        
        while i<vector.length
            self.insertar(vector[i])
            i=i+1
        end
    end
    
    def insertar_muchos_final(vector) #inserta muchos por el inicio
        i=0
        
        while i<vector.length
            self.insertar_final(vector[i])
            i=i+1
        end
    end
    
    def extraer #extrae por el inicio
        if(is_empty())
            return nil
        elsif
            node_aux = @inicio
            @inicio = @inicio.next
            node_aux.next = nil
            node_aux.previo = nil
            return node_aux.value
        end
    end
    
    def extraer_final
        if(is_empty())
            return nil
        elsif
            node_aux = @final
            @final = @final.previo
            if(@final!=nil)
                @final.next=nil
            elsif
                @inicio=nil
            end
            return node_aux.value
        end
    end
    
    attr_accessor :inicio
    attr_accessor :final
end

