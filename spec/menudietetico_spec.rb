require "spec_helper"
require "menudietetico/menudietetico"
require "menudietetico/menu.rb"

describe Menudietetico do

  it "has a version number" do
    expect(Menudietetico::VERSION).not_to be nil
  end

  it "does something useful" do
    expect(true).to eq(true)
  end

end

############# PRUEBAS PARA LOS NODOS #############

describe Node do
  
  before :each do
    @nodo2 = Node.new(2, nil, nil)
    @nodo1 = Node.new(1, nil, nil)
  end
  
  context "Inicialización del nodo" do
  
    it "Los nodos deben tener un valor" do
      expect(@nodo1.value).to eq(1)
      expect(@nodo2.value).to eq(2)
    end
    
    it "Deben tener un siguiente o un null" do
      expect(@nodo1.next).to eq(nil)
      expect(@nodo2.next).to eq(nil)
    end
    
    it "Deben tener un previo o un null" do
      expect(@nodo1.previo).to eq(nil)
      expect(@nodo2.previo).to eq(nil)
    end
    
  end
end


describe Lista do
  
  before :each do
    
    #### LAS COMIDAS ####
    
      ######## DESAYUNO ########
      t=['DESAYUNO' , '(15%)']
      i=['Leche desnatada', 'Cacao instantaneo', 'Cereales de desayuno en hojuelas', 'Almendras laminadas (10 unidades)']
      porciones=['1 vaso', ' 1 c/sopera', '1 bol pequeño', '2 c/soperas']
      gramos=['200 ml', '10 g', ' 40 g', '10 g']
      porcentajes=['V.C.T | % 288,0 kcal |',' 17% ', ' 21% ', ' 62% ']
      @desayuno = Menu.new(t,i,porciones,gramos,porcentajes)
      
      
      ######## MEDIA MAÑANA ########
      t=['MEDIA MAÑANA' , '(10%)']
      i=['Cerezas', 'Galletas bifidus con sesamo']
      porciones=['10-12 unidades medianas', '4 unidades']
      gramos=['120 g', '40 g']
      porcentajes=['V.C.T | % 255,5 kcal |',' 7% ', ' 24% ', ' 69% ']
      @mediaManana = Menu.new(t,i,porciones,gramos,porcentajes)
      
      ######## ALMUERZO ########
      t=['ALMUERZO' , '(30-35%)']
      i=['Macarrones con salsa tomate y queso', 'Escalope de ternera', 'Ensalada basica con zanahoria rallada', 'Mandarina', 'Pan de trigo integral']
      porciones=['1 1/2 cucharon', '1 bistec mediano', 'guarnicion de', '1 grande', '1 rodaja']
      gramos=['200gr', '100gr', '120gr', '180gr', '20gr']
      porcentajes=['V.C.T | % 785,9 kcal |',' 19% ', ' 34% ', ' 47% ']
      @almuerzo = Menu.new(t,i,porciones,gramos,porcentajes)
      
      
      ######## MERIENDA ########
      t=['MERIENDA' , '(15%)']
      i=['Galletas de leche con chocolate y yogur', 'Flan de vainilla sin huevo']
      porciones=['4 unidades', '1 unidad']
      gramos=['46 g', '110 g']
      porcentajes=['V.C.T | % 313,6 kcal |',' 10% ', ' 30% ', ' 60% ']
      @merienda = Menu.new(t,i,porciones,gramos,porcentajes)
      
      
      ######## CENA ########
      t=['CENA' , '(25% - 30%)']
      i=['Crema de bubango', 'Tortilla campesina con espinacas', 'Tomate en dados con atun', 'Piña natural o en su jugo picada', 'Pan de trigo integral']
      porciones=['2 cucharones', '1 cuña grande', '5 a 6 c/soperas', '5 c/soperas', '1 rodaja']
      gramos=['200 g', '150 g', '150 g', '120 g', '20 g']
      porcentajes=['V.C.T | % 561,6 kcal |',' 19% ', ' 40% ', ' 41% ']
      @cena = Menu.new(t,i,porciones,gramos,porcentajes)
      
      
      ### LISTA PARA LAS COMIDAS ###
       
      @lista1=Lista.new()
      
  end
  
  
  ##################################################### PRUEBAS PARA LAS COMIDAS #####################################################
  
  context "Inicialización de la lista1" do
    it "Debe existir un nodo inicial null" do
      expect(@lista1.get_inicio).to eq(nil)
    end
    
    it "Debe existir un nodo final null para lista1" do
      expect(@lista1.get_final).to eq(nil)
    end
  end
  
  context "Pruebas para los métodos con lista1" do
    it "Comprobar si la lista está vacía" do
      expect(@lista1.is_empty).to eq(true)
    end
    
     it "Insertar un elemento en la lista1" do
      @lista1.insertar(@merienda)
      
      expect(@lista1.get_inicio().value.to_s).to eq("MERIENDA (15%)\n" +
        "- Galletas de leche con chocolate y yogur, 4 unidades, 46 g\n" +
        "- Flan de vainilla sin huevo, 1 unidad, 110 g\n" +
        "V.C.T | % 313,6 kcal | 10% - 30% - 60% \n")
    end
    
    it "Extraer un valor de la lista1" do
      @lista1.insertar(@desayuno)
        expect(@lista1.extraer().to_s).to eq("DESAYUNO (15%)\n" +
        "- Leche desnatada, 1 vaso, 200 ml\n" +
        "- Cacao instantaneo,  1 c/sopera, 10 g\n" +
        "- Cereales de desayuno en hojuelas, 1 bol pequeño,  40 g\n"+
        "- Almendras laminadas (10 unidades), 2 c/soperas, 10 g\n"+
        "V.C.T | % 288,0 kcal | 17% - 21% - 62% \n")
    end
    
     it "Extraer un valor de la lista1 por el final" do
      @lista1.insertar_muchos([@desayuno, @almuerzo])
        expect(@lista1.extraer_final().to_s).to eq("DESAYUNO (15%)\n" +
        "- Leche desnatada, 1 vaso, 200 ml\n" +
        "- Cacao instantaneo,  1 c/sopera, 10 g\n" +
        "- Cereales de desayuno en hojuelas, 1 bol pequeño,  40 g\n"+
        "- Almendras laminadas (10 unidades), 2 c/soperas, 10 g\n"+
        "V.C.T | % 288,0 kcal | 17% - 21% - 62% \n")
    end
    
    it "Se pueden insertar varios elementos por el inicio" do
           
        expect(@lista1.is_empty()).to eq(true)
                
        @lista1.insertar_muchos([@merienda, @almuerzo])
        
               
        expect(@lista1.is_empty()).to eq(false)
        
           
        expect(@lista1.extraer().to_s).to eq("ALMUERZO (30-35%)\n" +
        "- Macarrones con salsa tomate y queso, 1 1/2 cucharon, 200gr\n" +
        "- Escalope de ternera, 1 bistec mediano, 100gr\n" +
        "- Ensalada basica con zanahoria rallada, guarnicion de, 120gr\n" +
        "- Mandarina, 1 grande, 180gr\n" +
        "- Pan de trigo integral, 1 rodaja, 20gr\n" +
        "V.C.T | % 785,9 kcal | 19% - 34% - 47% \n")
        
        expect(@lista1.extraer().to_s).to eq("MERIENDA (15%)\n" +
        "- Galletas de leche con chocolate y yogur, 4 unidades, 46 g\n" +
        "- Flan de vainilla sin huevo, 1 unidad, 110 g\n" +
        "V.C.T | % 313,6 kcal | 10% - 30% - 60% \n")
        expect(@lista1.is_empty()).to eq(true)
    end
    
    it "Insertar un elemento en la lista1 por el final" do
      @lista1.insertar_final(@merienda)
      
      expect(@lista1.get_final().value.to_s).to eq("MERIENDA (15%)\n" +
        "- Galletas de leche con chocolate y yogur, 4 unidades, 46 g\n" +
        "- Flan de vainilla sin huevo, 1 unidad, 110 g\n" +
        "V.C.T | % 313,6 kcal | 10% - 30% - 60% \n")
    end
    
    it "Se pueden insertar varios elementos por el final" do
           
        expect(@lista1.is_empty()).to eq(true)
                
        @lista1.insertar_muchos_final([@almuerzo, @merienda])
        
               
        expect(@lista1.is_empty()).to eq(false)
        
         expect(@lista1.extraer_final().to_s).to eq("MERIENDA (15%)\n" +
        "- Galletas de leche con chocolate y yogur, 4 unidades, 46 g\n" +
        "- Flan de vainilla sin huevo, 1 unidad, 110 g\n" +
        "V.C.T | % 313,6 kcal | 10% - 30% - 60% \n")
        
           
        expect(@lista1.extraer_final().to_s).to eq("ALMUERZO (30-35%)\n" +
        "- Macarrones con salsa tomate y queso, 1 1/2 cucharon, 200gr\n" +
        "- Escalope de ternera, 1 bistec mediano, 100gr\n" +
        "- Ensalada basica con zanahoria rallada, guarnicion de, 120gr\n" +
        "- Mandarina, 1 grande, 180gr\n" +
        "- Pan de trigo integral, 1 rodaja, 20gr\n" +
        "V.C.T | % 785,9 kcal | 19% - 34% - 47% \n")
        
        expect(@lista1.is_empty()).to eq(true)
    end
  end


end

describe Grupos_alimentos do
  before :each do
    
    ######## CENA ########
    t=['CENA' , '(25% - 30%)']
    i=['Crema de bubango', 'Tortilla campesina con espinacas', 'Tomate en dados con atun', 'Piña natural o en su jugo picada', 'Pan de trigo integral']
    porciones=['2 cucharones', '1 cuña grande', '5 a 6 c/soperas', '5 c/soperas', '1 rodaja']
    gramos=['200 g', '150 g', '150 g', '120 g', '20 g']
    porcentajes=['V.C.T | % 561,6 kcal |',' 19% ', ' 40% ', ' 41% ']
    @grupo1 = Grupos_alimentos.new('verduras y otras hortalizas', t, i, porciones, gramos, porcentajes)
    
    t=['DESAYUNO' , '(15%)']
    i=['Leche desnatada', 'Cacao instantaneo', 'Cereales de desayuno en hojuelas', 'Almendras laminadas (10 unidades)']
    porciones=['1 vaso', ' 1 c/sopera', '1 bol pequeño', '2 c/soperas']
    gramos=['200 ml', '10 g', ' 40 g', '10 g']
    porcentajes=['V.C.T | % 288,0 kcal |',' 17% ', ' 21% ', ' 62% ']
    @grupo2 = Grupos_alimentos.new('leche, huevos, pescado, carne y frutos secos', t, i, porciones, gramos, porcentajes)
    
    ################# GRUPO DE cereales,legumbres y feculas y frutas #################
    t=['ALMUERZO' , '(30-35%)']
    i=['Macarrones con salsa tomate y queso', 'Escalope de ternera', 'Ensalada basica con zanahoria rallada', 'Mandarina', 'Pan de trigo integral']
    porciones=['1 1/2 cucharon', '1 bistec mediano', 'guarnicion de', '1 grande', '1 rodaja']
    gramos=['200gr', '100gr', '120gr', '180gr', '20gr']
    porcentajes=['V.C.T | % 785,9 kcal |',' 19% ', ' 34% ', ' 47% ']
    @grupo3 = Grupos_alimentos.new('cereales,legumbres y feculas y frutas', t, i, porciones, gramos, porcentajes)
  
  end
  context "Inicialización de variables" do
    it "Debe contener un grupo de alimentos" do
      expect(@grupo1.texto).to eq('verduras y otras hortalizas')
      expect(@grupo2.texto).to eq('leche, huevos, pescado, carne y frutos secos')
      expect(@grupo3.texto).to eq('cereales,legumbres y feculas y frutas')
    
    end
  end
 
 context "Mostrar el resultado" do 
    it "Imprime correctamente formateados los resultados" do
      expect(@grupo3.to_s).to eq("ALMUERZO (30-35%)\n" +
          "- Macarrones con salsa tomate y queso, 1 1/2 cucharon, 200gr\n" +
          "- Escalope de ternera, 1 bistec mediano, 100gr\n" +
          "- Ensalada basica con zanahoria rallada, guarnicion de, 120gr\n" +
          "- Mandarina, 1 grande, 180gr\n" +
          "- Pan de trigo integral, 1 rodaja, 20gr\n" +
          "V.C.T | % 785,9 kcal | 19% - 34% - 47% \n" +
          "Menu del grupo: cereales,legumbres y feculas y frutas\n")
          
      expect(@grupo2.to_s).to eq("DESAYUNO (15%)\n" +
          "- Leche desnatada, 1 vaso, 200 ml\n" +
          "- Cacao instantaneo,  1 c/sopera, 10 g\n" +
          "- Cereales de desayuno en hojuelas, 1 bol pequeño,  40 g\n"+
          "- Almendras laminadas (10 unidades), 2 c/soperas, 10 g\n"+
          "V.C.T | % 288,0 kcal | 17% - 21% - 62% \n" +
          "Menu del grupo: leche, huevos, pescado, carne y frutos secos\n")
          
      expect(@grupo1.to_s).to eq("CENA (25% - 30%)\n" +
          "- Crema de bubango, 2 cucharones, 200 g\n" +
          "- Tortilla campesina con espinacas, 1 cuña grande, 150 g\n" +
          "- Tomate en dados con atun, 5 a 6 c/soperas, 150 g\n"+
          "- Piña natural o en su jugo picada, 5 c/soperas, 120 g\n"+
          "- Pan de trigo integral, 1 rodaja, 20 g\n"+
          "V.C.T | % 561,6 kcal | 19% - 40% - 41% \n" +
          "Menu del grupo: verduras y otras hortalizas\n")
    
    end
  end
  
  context "Comprobaciones de la clase, pertenencia a la jerarquía y tipo" do
    it "Comprobar si es un objeto de la clase" do
      expect(@grupo3.instance_of?Grupos_alimentos).to eq(true)
      expect(@grupo3.instance_of?Edades).to eq(false)
      expect(@grupo3.instance_of?Object).to eq(false)
      expect(@grupo3.instance_of?BasicObject).to eq(false)
    end
    
    it "Comprobaciones de la herencia" do
      expect(@grupo3.is_a?Menu).to eq(true)
      expect(@grupo3.is_a?Object).to eq(true)
      expect(@grupo3.is_a?BasicObject).to eq(true)
      expect(@grupo3.is_a?Edades).to eq(false)
      
    end
    
    it "Comprobación del tipo" do
     expect(@grupo3.respond_to?('to_s')).to eq(true)
     expect(@grupo3.respond_to?('get_proteinas')).to eq(true)
     expect(@grupo3.respond_to?('get_grasas')).to eq(true)

    end
  
  end
end

describe Edades do
  before :each do
    t=['MERIENDA' , '(15%)']
    i=['Galletas de leche con chocolate y yogur', 'Flan de vainilla sin huevo']
    porciones=['4 unidades', '1 unidad']
    gramos=['46 g', '110 g']
    porcentajes=['V.C.T | % 313,6 kcal |',' 10% ', ' 30% ', ' 60% ']
    @edad1 = Edades.new('4 a 8 años', t, i, porciones, gramos, porcentajes)
    
    t=['DESAYUNO' , '(15%)']
    i=['Leche desnatada', 'Cacao instantaneo', 'Cereales de desayuno en hojuelas', 'Almendras laminadas (10 unidades)']
    porciones=['1 vaso', ' 1 c/sopera', '1 bol pequeño', '2 c/soperas']
    gramos=['200 ml', '10 g', ' 40 g', '10 g']
    porcentajes=['V.C.T | % 288,0 kcal |',' 17% ', ' 21% ', ' 62% ']
    @edad2 = Edades.new('9 a 13 años', t, i, porciones, gramos, porcentajes)
    
    t=['ALMUERZO' , '(30-35%)']
    i=['Macarrones con salsa tomate y queso', 'Escalope de ternera', 'Ensalada basica con zanahoria rallada', 'Mandarina', 'Pan de trigo integral']
    porciones=['1 1/2 cucharon', '1 bistec mediano', 'guarnicion de', '1 grande', '1 rodaja']
    gramos=['200gr', '100gr', '120gr', '180gr', '20gr']
    porcentajes=['V.C.T | % 785,9 kcal |',' 19% ', ' 34% ', ' 47% ']
    @edad3 = Edades.new('14 a 18 años', t, i, porciones, gramos, porcentajes)
  end
  
  context "Inicialización de variables" do
    it "Debe contener un rango de edad" do
      expect(@edad1.texto).to eq('4 a 8 años')
      expect(@edad2.texto).to eq('9 a 13 años')
      expect(@edad3.texto).to eq('14 a 18 años')
    
    end
  end
  
  context "Mostrar el resultado" do
    it "Imprime correctamente formateados los resultados" do
      expect(@edad1.to_s).to eq("MERIENDA (15%)\n" +
          "- Galletas de leche con chocolate y yogur, 4 unidades, 46 g\n" +
          "- Flan de vainilla sin huevo, 1 unidad, 110 g\n" +
          "V.C.T | % 313,6 kcal | 10% - 30% - 60% \n" +
          "Edad: 4 a 8 años\n")
          
      expect(@edad2.to_s).to eq("DESAYUNO (15%)\n" +
          "- Leche desnatada, 1 vaso, 200 ml\n" +
          "- Cacao instantaneo,  1 c/sopera, 10 g\n" +
          "- Cereales de desayuno en hojuelas, 1 bol pequeño,  40 g\n"+
          "- Almendras laminadas (10 unidades), 2 c/soperas, 10 g\n"+
          "V.C.T | % 288,0 kcal | 17% - 21% - 62% \n" +
          "Edad: 9 a 13 años\n")
          
      expect(@edad3.to_s).to eq("ALMUERZO (30-35%)\n" +
          "- Macarrones con salsa tomate y queso, 1 1/2 cucharon, 200gr\n" +
          "- Escalope de ternera, 1 bistec mediano, 100gr\n" +
          "- Ensalada basica con zanahoria rallada, guarnicion de, 120gr\n" +
          "- Mandarina, 1 grande, 180gr\n" +
          "- Pan de trigo integral, 1 rodaja, 20gr\n" +
          "V.C.T | % 785,9 kcal | 19% - 34% - 47% \n" +
          "Edad: 14 a 18 años\n")
    end
  end
  
  context "Comprobaciones de la clase, pertenencia a la jerarquía y tipo" do
    it "Comprobar si es un objeto de la clase" do
      expect(@edad1.instance_of?Grupos_alimentos).to eq(false)
      expect(@edad1.instance_of?Edades).to eq(true)
      expect(@edad1.instance_of?Object).to eq(false)
      expect(@edad1.instance_of?BasicObject).to eq(false)
    end
    
    it "Comprobaciones de la pertenencia a la jerarquía" do
      expect(@edad1.is_a?Menu).to eq(true)
      expect(@edad1.is_a?Object).to eq(true)
      expect(@edad1.is_a?BasicObject).to eq(true)
      expect(@edad1.is_a?Grupos_alimentos).to eq(false)
    end
    
    it "Comprobación del tipo" do
     expect(@edad1.respond_to?('to_s')).to eq(true)
     expect(@edad1.respond_to?('get_proteinas')).to eq(true)
     expect(@edad1.respond_to?('get_grasas')).to eq(true)
    end
  end

end